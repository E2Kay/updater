#ifndef GAJIMDIALOG_H
#define GAJIMDIALOG_H

#include "jsonparser.h"
#include "downloader.h"
#include "extractor.h"
#include "copier.h"

#include <QDialog>
#include <QFileDialog>
#include <QStatusBar>

namespace Ui {
class GajimDialog;
}

class GajimDialog : public QDialog
{
    Q_OBJECT

public:
    explicit GajimDialog(QWidget *parent = 0);
    ~GajimDialog();

private:
    Ui::GajimDialog *ui;

    JsonParser jparser;
    Downloader dler;
    Extractor extrer;
    Copier cpier;

    QStatusBar *statusBar;

    QString version;
    QString zipname;
    QString homePath;
    QString destination;
    QString dialogDestination;

    void getSmileFolder();
    void disableInstallButton();

public slots:
    void setStatusBarText(QString);

private slots:
    void on_versionBtn_clicked();
    void on_browseBtn_clicked();
    void on_installBtn_clicked();

    void setNewestSmilepackVersion();
    void enableInstallButton();
    void startExtract(QString);
    void startCopying(QString);
    void setSmileLocation(QString);

signals:
    void gajimVersionChanged();
    void download(QUrl);
    void startextracting(QString);
    void startcopying(QString, QString, bool, bool);
    void locationChanged();
};

#endif // GAJIMDIALOG_H
