#ifndef DOWNLOADER_H
#define DOWNLOADER_H

#include <QFile>
#include <QFileInfo>
#include <QList>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QStringList>
#include <QTimer>
#include <QUrl>
#include <QDir>

class Downloader : public QObject
{
    Q_OBJECT
    QNetworkAccessManager manager;
    QList<QNetworkReply *> currentDownloads;

public:
   Downloader();

   QString saveFileName(const QUrl &url);
   bool saveToDisk(const QString &fullpath, QIODevice *data);

private:
   QString fullpath;

public slots:
   void doDownload(const QUrl &url);
   void downloadFinished(QNetworkReply *reply);

signals:
   void downloadDone(QString);
   void addToStatusBar(QString);
};

#endif // DOWNLOADER_H
