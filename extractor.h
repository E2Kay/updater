#ifndef EXTRACTOR_H
#define EXTRACTOR_H

#include "quazip/JlCompress.h"

#include <QFuture>
#include <QtConcurrent/QtConcurrent>
#include <QThread>

class Extractor: public QObject
{
    Q_OBJECT

public:
    Extractor();



private:
    void extract(QString filepath);

public slots:
    void runExtractThread(QString filepath);

signals:
    void extracted(QString);
    void addToStatusBar(QString);
};

#endif // EXTRACTOR_H
