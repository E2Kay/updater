#ifndef WOTDIALOG_H
#define WOTDIALOG_H

#include "jsonparser.h"
#include "downloader.h"
#include "extractor.h"
#include "copier.h"

#include <QDialog>
#include <QFileDialog>
#include <QStatusBar>

namespace Ui {
class WotDialog;
}

class WotDialog : public QDialog
{
    Q_OBJECT

public:
    explicit WotDialog(QWidget *parent = 0);
    ~WotDialog();

private:
    Ui::WotDialog *ui;

    JsonParser jparser;
    Downloader dler;
    Extractor extrer;
    Copier cpier;

    QString wotLocation;

    QString version;
    QString zipname;

    QStatusBar *statusBar;

    void disableInstallButton();

public slots:
    void setStatusBarText(QString text);


private slots:
    void on_wotVersionBtn_clicked();
    void setNewestModpackVersion();
    void on_wotBrowseBtn_clicked();
    void on_wotInstallBtn_clicked();
    void startExtract(QString target);
    void startCopying(QString source);
    void enableInstallButton();

signals:
   void download(QUrl);
   void startextracting(QString);
   void startcopying(QString, QString, bool, bool);
   void wotLocationChanged();
   void wotVersionChanged();
};

#endif // WOTDIALOG_H
