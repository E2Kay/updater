#include "gajimdialog.h"
#include "ui_gajimdialog.h"

#include <QDebug>

GajimDialog::GajimDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::GajimDialog)
{
    ui->setupUi(this);

    statusBar = new QStatusBar();
    ui->verticalLayout_6->addWidget(statusBar);
    this->statusBar->setSizeGripEnabled(false);

    connect(this, SIGNAL(gajimVersionChanged()), this, SLOT(enableInstallButton()));
    connect(this, SIGNAL(download(QUrl)), &dler, SLOT(doDownload(QUrl)));
    connect(&dler, SIGNAL(downloadDone(QString)), this, SLOT(startExtract(QString)));
    connect(this, SIGNAL(startextracting(QString)), &extrer, SLOT(runExtractThread(QString)));
    connect(&extrer, SIGNAL(extracted(QString)), this, SLOT(startCopying(QString)));
    connect(this, SIGNAL(startcopying(QString,QString, bool, bool)), &cpier, SLOT(runCopyThread(QString,QString, bool, bool)));
    connect(this, SIGNAL(locationChanged()), this, SLOT(enableInstallButton()));
    connect(&cpier, SIGNAL(enableInstallBtn()), this, SLOT(enableInstallButton()));

    // addToStatusBar
    connect(&cpier, SIGNAL(addToStatusBar(QString)), this, SLOT(setStatusBarText(QString)));
    connect(&dler, SIGNAL(addToStatusBar(QString)), this, SLOT(setStatusBarText(QString)));
    connect(&extrer, SIGNAL(addToStatusBar(QString)), this, SLOT(setStatusBarText(QString)));

    getSmileFolder();
    setNewestSmilepackVersion();
}

GajimDialog::~GajimDialog()
{
    delete ui;
}

void GajimDialog::getSmileFolder()
{
    homePath = QDir::homePath();

    #ifdef Q_OS_WIN
        destination = homePath + "/AppData/Roaming/Gajim/Emoticons";
        setSmileLocation(destination);
        emit locationChanged();
    #endif

    #ifdef Q_OS_UNIX
        destination = homePath + "/.local/share/gajim/emoticons";
        setSmileLocation(destination);
        emit locationChanged();
#endif
}

void GajimDialog::disableInstallButton()
{
    ui->installBtn->setEnabled(false);
}

void GajimDialog::setStatusBarText(QString text)
{
    statusBar->showMessage(text);
}

void GajimDialog::setNewestSmilepackVersion()
{
    zipname = jparser.parseJson("https://eddykk.com/api/latest/gajim", "content");
    if(zipname != NULL)
    {
        version = zipname.mid(6);
        version.chop(10);
        ui->webVersionLabel->setText(version);
        setStatusBarText("Version information updated");
        emit gajimVersionChanged();
    }
}

void GajimDialog::enableInstallButton()
{
    if(ui->gajimLocationLbl->text() != "Not selected :( Press Browse and locate smile folder"
    && ui->webVersionLabel->text() != "Could not connect to EddyKK server")
    {
        ui->installBtn->setEnabled(true);
    }else{
        ui->installBtn->setEnabled(false);
    }
}

void GajimDialog::startExtract(QString target)
{
    setStatusBarText("Extracting started");
    emit startextracting(target);
}

void GajimDialog::startCopying(QString source)
{
    QString finalDestiantion = ui->gajimLocationLbl->text();
    setStatusBarText("Copying started");
    emit startcopying(source, finalDestiantion, false, false);

}

void GajimDialog::setSmileLocation(QString path)
{
    ui->gajimLocationLbl->setText(path);
    setStatusBarText("Gajim's smile location is set");
}

void GajimDialog::on_versionBtn_clicked()
{
    setNewestSmilepackVersion();
}

void GajimDialog::on_browseBtn_clicked()
{
    dialogDestination = QFileDialog::getExistingDirectory(this, tr("Open smile installation directory"), destination);

    if (dialogDestination != NULL)
    {
        setSmileLocation(dialogDestination);
        emit locationChanged();
    }
    qDebug() << "Destination folder selected: " << dialogDestination;
}

void GajimDialog::on_installBtn_clicked()
{
    disableInstallButton();

    QUrl url = "https://eddykk.com/smiley/gajim/" + zipname;

    qDebug() << "Installation started";
    setStatusBarText("Installation started");
    emit download(url);

}
