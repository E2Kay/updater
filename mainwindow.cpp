#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(this, SIGNAL(wotButtonClicked()), &wotDialog, SLOT(setNewestModpackVersion()));

    ui->wotButton->setToolTip("Install WoT Modpack");
    ui->pidginButton->setToolTip("Install Pidgin Smilepack");
    ui->gajimButton->setToolTip("Install Gajim Smilepack");

    currentVersion = 1;
    checkForUpdate();

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::checkForUpdate()
{
    zipname = jparser->parseJson("https://eddykk.com/api/latest/updater", "content");
    if(zipname != NULL)
    {
        version = zipname.mid(17);
        version.chop(4);

        int v = version.toInt();

        if (currentVersion < v)
        {
            updated = new UpdateDialog(this);
            updated->show();
        }
    }
}

void MainWindow::on_wotButton_clicked()
{
    WotDialog *wotDialog = new WotDialog(this);
    wotDialog->show();
    emit wotButtonClicked();
}


void MainWindow::on_pidginButton_clicked()
{
    PidginDialog *piDialog = new PidginDialog(this);
    piDialog->show();
}

void MainWindow::on_gajimButton_clicked()
{
    GajimDialog *gaDialog = new GajimDialog(this);
    gaDialog->show();
}
