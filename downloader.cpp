#include "downloader.h"

Downloader::Downloader()
{
    connect(&manager, SIGNAL(finished(QNetworkReply*)),this, SLOT(downloadFinished(QNetworkReply*)));
}

// Constructs a QList of QNetworkReply
void Downloader::doDownload(const QUrl &url)
{
    emit addToStatusBar("Downloading");
    qDebug() << "Downloading from: " << url;

    QNetworkRequest request(url);
    QNetworkReply *reply = manager.get(request);

    // List of reply
    currentDownloads.append(reply);
}

QString Downloader::saveFileName(const QUrl &url)
{
    QString path = url.path();
    QString basename = QFileInfo(path).fileName();

    if (basename.isEmpty()) {
        basename = "download.zip";
    }

    QString tempdir = QDir::tempPath();
    fullpath = tempdir + "/" + basename;

    return fullpath;
}

void Downloader::downloadFinished(QNetworkReply *reply)
{
    QUrl url = reply->url();

    if (reply->error()) {
        qDebug() << "Download of %s failed: %s\n" << url.toEncoded().constData() << qPrintable(reply->errorString());

    } else {
        QString filename = saveFileName(url);

        if (saveToDisk(filename, reply))
            qDebug() << "Download done";
            emit downloadDone(fullpath);
    }
    currentDownloads.removeAll(reply);
    reply->deleteLater();
}

bool Downloader::saveToDisk(const QString &path, QIODevice *reply)
{
    QFile file(path);
    qDebug() << "Saving file to: " << path;

    if (!file.open(QIODevice::WriteOnly)) {
        qDebug() << "Could not open %s for writing: %s\n" << qPrintable(path) << qPrintable(file.errorString());
        return false;
    }

    file.write(reply->readAll());
    file.close();
    return true;
}
