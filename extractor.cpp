#include "extractor.h"

#include <QDebug>


Extractor::Extractor()
{

}

void Extractor::runExtractThread(QString filepath)
{
    QFuture<void> extractThread = QtConcurrent::run(this, &Extractor::extract, filepath);
}

void Extractor::extract(QString filepath)
{
    QString destinationDir = filepath;
    destinationDir.chop(4);

    QStringList list = JlCompress::extractDir(filepath, destinationDir);
//    JlCompress *asd;
//    asd->extractDir(filepath, destinationDir);

    foreach(QString item, list)
    {
        qDebug() << "Extracted: " << item;
    }

    qDebug() << "Extraction done to: " << destinationDir;
    emit extracted(destinationDir);
    emit addToStatusBar("Extracted!");

}
