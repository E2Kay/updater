#include "updatedialog.h"
#include "ui_updatedialog.h"

UpdateDialog::UpdateDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UpdateDialog)
{
    ui->setupUi(this);

    ui->urlLabel->setOpenExternalLinks(true);
}

UpdateDialog::~UpdateDialog()
{
    delete ui;
}
