#include "wotdialog.h"
#include "ui_wotdialog.h"

#include <QDebug>

WotDialog::WotDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WotDialog)
{
    ui->setupUi(this);

    statusBar = new QStatusBar();
    ui->verticalLayout_6->addWidget(statusBar);
    this->statusBar->setSizeGripEnabled(false);

    connect(this, SIGNAL(download(QUrl)), &dler, SLOT(doDownload(QUrl)));
    connect(&dler, SIGNAL(downloadDone(QString)), this, SLOT(startExtract(QString)));
    connect(this, SIGNAL(startextracting(QString)), &extrer, SLOT(runExtractThread(QString)));
    connect(&extrer, SIGNAL(extracted(QString)), this, SLOT(startCopying(QString)));
    connect(this, SIGNAL(startcopying(QString,QString, bool, bool)), &cpier, SLOT(runCopyThread(QString,QString, bool, bool)));
    connect(this, SIGNAL(wotLocationChanged()), this, SLOT(enableInstallButton()));
    connect(this, SIGNAL(wotVersionChanged()), this, SLOT(enableInstallButton()));
    connect(&cpier, SIGNAL(enableInstallBtn()), this, SLOT(enableInstallButton()));

    // addToStatusBar
    connect(&cpier, SIGNAL(addToStatusBar(QString)), this, SLOT(setStatusBarText(QString)));
    connect(&dler, SIGNAL(addToStatusBar(QString)), this, SLOT(setStatusBarText(QString)));
    connect(&extrer, SIGNAL(addToStatusBar(QString)), this, SLOT(setStatusBarText(QString)));

    setNewestModpackVersion();
}

WotDialog::~WotDialog()
{
    delete ui;
}

void WotDialog::on_wotVersionBtn_clicked()
{
    setNewestModpackVersion();
}

void WotDialog::setNewestModpackVersion()
{
    zipname = jparser.parseJson("https://eddykk.com/api/latest/wot", "content");
    if(zipname != NULL)
    {
        version = zipname.mid(18);
        version.chop(4);
        ui->webVersionLabel->setText(version);
        setStatusBarText("Version information updated");
        emit wotVersionChanged();
    }
}

void WotDialog::on_wotBrowseBtn_clicked()
{
    wotLocation = QFileDialog::getOpenFileName(this, tr("Select WOTLauncher.exe"), "/", tr("WOTLauncher.exe (WOTLauncher.exe)"));

    if (wotLocation != NULL)
    {
        wotLocation.chop(16);
        ui->wotLocationLbl->setText(wotLocation);
        emit wotLocationChanged();
        setStatusBarText("WoT location is set");
    }
    qDebug() << "WoT location is se to: " << wotLocation;
}

void WotDialog::on_wotInstallBtn_clicked()
{
    disableInstallButton();

    QUrl url = "https://eddykk.com/wot/" + zipname;

    qDebug() << "Installation started";
    setStatusBarText("Installation started");

    emit download(url);
}

void WotDialog::startExtract(QString target)
{
    emit startextracting(target);
    qDebug() << "Extracting started";
    setStatusBarText("Extracting started");
}

void WotDialog::startCopying(QString source)
{
    emit startcopying(source, wotLocation, ui->wotCleanCBox->isChecked(), ui->wotBackupCBox->isChecked());
}

void WotDialog::enableInstallButton()
{
    if(ui->wotLocationLbl->text() != "Not selected :( Press Browse and locate WoT client"
        && ui->webVersionLabel->text() != "Could not connect to EddyKK server")
    {
        ui->wotInstallBtn->setEnabled(true);
    }else{
        ui->wotInstallBtn->setEnabled(false);
    }
}

void WotDialog::setStatusBarText(QString text)
{
    statusBar->showMessage(text);
}

void WotDialog::disableInstallButton()
{
    ui->wotInstallBtn->setEnabled(false);
}
