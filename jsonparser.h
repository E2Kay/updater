#ifndef JSONPARSER_H
#define JSONPARSER_H

#include <QVariant>
#include <QCoreApplication>
#include <QDebug>
//#include <QtWebKitWidgets/QWebFrame>
//#include <QtWebKitWidgets/QWebPage>
//#include <QtWebKitWidgets/QWebView>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include <QUrlQuery>
//#include <QWebSettings>
#include <QVariant>
#include <QJsonValue>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QJsonArray>


class JsonParser
{
public:
    JsonParser();
    QString parseJson(QString const& url, const QString &string);

};

#endif // JSONPARSER_H
