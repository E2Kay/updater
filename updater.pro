§#-------------------------------------------------
#
# Project created by QtCreator 2015-06-11T19:13:00
#
#-------------------------------------------------

QT       += core gui
QT       -= gui
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = updater
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    jsonparser.cpp \
    wotdialog.cpp \
    downloader.cpp \
    extractor.cpp \
    copier.cpp \
    updatedialog.cpp \
    pidgindialog.cpp \
    gajimdialog.cpp

HEADERS  += mainwindow.h \
    jsonparser.h \
    wotdialog.h \
    downloader.h \
    extractor.h \
    copier.h \
    updatedialog.h \
    pidgindialog.h \
    gajimdialog.h \
    ui_gajimdialog.h \
    ui_mainwindow.h \
    ui_pidgindialog.h \
    ui_updatedialog.h \
    ui_wotdialog.h \

FORMS    += mainwindow.ui \
    wotdialog.ui \
    updatedialog.ui \
    pidgindialog.ui \
    gajimdialog.ui

RESOURCES += \
    resource.qrc

DISTFILES += \
    README.md

INCLUDEPATH += $$PWD/quazip
LIBS += -L$$PWD/libs/ -lz
LIBS += -L$$PWD/libs/ -lquazip
