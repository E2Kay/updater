#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "wotdialog.h"
#include "pidgindialog.h"
#include "gajimdialog.h"
#include "jsonparser.h"
#include "updatedialog.h"


#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void checkForUpdate();

private:
    Ui::MainWindow *ui;

    WotDialog wotDialog;
    PidginDialog piDialog;
    GajimDialog gaDialog;

    JsonParser *jparser;
    UpdateDialog *updated;

    QString zipname;
    QString version;

    int currentVersion;

private slots:
    void on_wotButton_clicked();

    void on_pidginButton_clicked();

    void on_gajimButton_clicked();

signals:
    void wotButtonClicked();
};

#endif // MAINWINDOW_H
