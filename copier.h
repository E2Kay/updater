#ifndef COPIER_H
#define COPIER_H

#include <QFuture>
#include <QtConcurrent/QtConcurrent>
#include <QProcess>

class Copier : public QObject
{
    Q_OBJECT

public:
    Copier();

public slots:
    void runCopyThread(QString source, QString destination, bool wipe, bool backup);
    void copy(QString source, QString destination, bool wipe, bool backup);

private slots:
    void backupError(QProcess::ProcessError);
    void cpError(QProcess::ProcessError);
    void cpFinished();

private:
    QString destination;

signals:
      void getWot();
      void addToStatusBar(QString);
      void enableInstallBtn();
};

#endif // COPIER_H
