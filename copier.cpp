#include "copier.h"

#include <QDebug>

Copier::Copier()
{


}

void Copier::runCopyThread(QString source, QString destination, bool wipe, bool backup)
{
    QFuture<void> copyThread = QtConcurrent::run(this, &Copier::copy, source, destination, wipe, backup);
}

void Copier::copy(QString source, QString destination, bool wipe, bool backup)
{
    //  IF WINDOWS
    #ifdef Q_OS_WIN


    QProcess backupxcopy;
    QProcess xcopy;

    qRegisterMetaType<QProcess::ExitStatus>("QProcess::ExitStatus");

    connect(&backupxcopy, SIGNAL(error(QProcess::ProcessError)),this, SLOT(backupError(QProcess::ProcessError)));
    connect(&xcopy, SIGNAL(error(QProcess::ProcessError)), this, SLOT(cpError(QProcess::ProcessError)));
    connect(&xcopy, SIGNAL(finished(int,QProcess::ExitStatus)), this, SLOT(cpFinished()));

    if(QDir().exists(destination) == false)
    {
        QDir().mkpath(destination);
        qDebug() << "Created path: " << destination;
    }

    if(backup)
    {
        qDebug() << "Backup started";
        emit addToStatusBar("Backup started");

        QStringList backupparameters;
        QString backupsource = destination + "/res_mods";
        QString backupdestination = destination + "/res_mods_backup";

        backupparameters << QLatin1String("/E") << QLatin1String("/Y") << QDir::toNativeSeparators(backupsource) << QDir::toNativeSeparators(backupdestination);

        QDir().mkdir(backupdestination);

        backupxcopy.start(QLatin1String("xcopy.exe"), backupparameters);

        if (!backupxcopy.waitForStarted() || !backupxcopy.waitForFinished())
        {
            qDebug() << "Could not start backuping :(";
            emit addToStatusBar("Could not start backuping :(");
        }
    }

    if(wipe)
    {
        QString removedestination = destination + "/res_mods";
        QDir directory = removedestination;

        if(directory.exists())
        {
            directory.removeRecursively();

            if(!directory.exists())
            {
                qDebug() << "Current mods removed";
                emit addToStatusBar("Current mods removed!");
            }

        }
    }

    QStringList copyparameters;
    copyparameters << QLatin1String("/E") << QLatin1String("/Y") << QDir::toNativeSeparators(source) << QDir::toNativeSeparators(destination);

    xcopy.start(QLatin1String("xcopy.exe"), copyparameters);

    if (!xcopy.waitForStarted() || !xcopy.waitForFinished())
    {
        qDebug() << "Could not start copying :(";
        emit addToStatusBar("Could not start copying :(");
    }

    #endif

    // IF UNIX
    #ifdef Q_OS_UNIX

    QProcess backupcp;
    QProcess cp;
    qRegisterMetaType<QProcess::ExitStatus>("QProcess::ExitStatus");

    connect(&backupcp, SIGNAL(error(QProcess::ProcessError)),this, SLOT(backupError(QProcess::ProcessError)));
    connect(&cp, SIGNAL(error(QProcess::ProcessError)), this, SLOT(cpError(QProcess::ProcessError)));
    connect(&cp, SIGNAL(finished(int,QProcess::ExitStatus)), this, SLOT(cpFinished()));

    if(QDir().exists(destination) == false)
    {
         QDir().mkpath(destination);
         qDebug() << "Created path: " << destination;
    }

    if(backup)
    {
        qDebug() << "Backup started";
        emit addToStatusBar("Backup started");

        QStringList backupparameters;
        QString backupsource = destination + "/res_mods";
        QString backupdestination = destination + "/res_mods_backup";

        backupparameters << QLatin1String("-a") << QDir::toNativeSeparators(backupsource) << QDir::toNativeSeparators(backupdestination);

        QDir().mkdir(backupdestination);

        backupcp.start(QLatin1String("cp"), backupparameters);

        if (!backupcp.waitForStarted() || !backupcp.waitForFinished())
        {
            qDebug() << "Could not start backuping :(";
            emit addToStatusBar("Could not start backuping :(");
        }
    }

    if(wipe)
    {
        QString removedestination = destination + "/res_mods";
        QDir directory = removedestination;

        if(directory.exists())
        {
            directory.removeRecursively();

            if(!directory.exists())
            {
                qDebug() << "Current mods removed";
                emit addToStatusBar("Current mods removed!");
            }

        }
    }

    QStringList copyparameters;
    QString sourceWithDot = source + "/.";

    copyparameters << QLatin1String("-a") << QDir::toNativeSeparators(sourceWithDot) << QDir::toNativeSeparators(destination);

    cp.start(QLatin1String("cp"), copyparameters);

    if (!cp.waitForStarted() || !cp.waitForFinished())
    {
        qDebug() << "Could not start copying :(";
        emit addToStatusBar("Could not start copying :(");
    }

    #endif

}

void Copier::backupError(QProcess::ProcessError error)
{
    qDebug() << "Error: " << error;

    emit addToStatusBar("Could not backup. Installation terminated.");
    emit enableInstallBtn();
}

void Copier::cpError(QProcess::ProcessError error)
{
    qDebug() << "Error: " << error;

    emit addToStatusBar("Could not copy content to destiantion folder. Installation terminated.");
    emit enableInstallBtn();
}

void Copier::cpFinished()
{
    qDebug() << "Copying is done";
    qDebug() << "Installation is done!";

    emit addToStatusBar("Installation is done!");
    emit enableInstallBtn();
}
