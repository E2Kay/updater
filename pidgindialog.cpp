#include "pidgindialog.h"
#include "ui_pidgindialog.h"

#include <QDebug>

PidginDialog::PidginDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PidginDialog)
{
    ui->setupUi(this);

    statusBar = new QStatusBar();
    ui->verticalLayout_6->addWidget(statusBar);
    this->statusBar->setSizeGripEnabled(false);

    connect(this, SIGNAL(pidginVersionChanged()), this, SLOT(enableInstallButton())); 
    connect(this, SIGNAL(download(QUrl)), &dler, SLOT(doDownload(QUrl)));
    connect(&dler, SIGNAL(downloadDone(QString)), this, SLOT(startExtract(QString)));
    connect(this, SIGNAL(startextracting(QString)), &extrer, SLOT(runExtractThread(QString)));
    connect(&extrer, SIGNAL(extracted(QString)), this, SLOT(startCopying(QString)));
    connect(this, SIGNAL(startcopying(QString,QString, bool, bool)), &cpier, SLOT(runCopyThread(QString,QString, bool, bool)));
    connect(this, SIGNAL(locationChanged()), this, SLOT(enableInstallButton()));
    connect(&cpier, SIGNAL(enableInstallBtn()), this, SLOT(enableInstallButton()));

    // addToStatusBar
    connect(&cpier, SIGNAL(addToStatusBar(QString)), this, SLOT(setStatusBarText(QString)));
    connect(&dler, SIGNAL(addToStatusBar(QString)), this, SLOT(setStatusBarText(QString)));
    connect(&extrer, SIGNAL(addToStatusBar(QString)), this, SLOT(setStatusBarText(QString)));

    getSmileFolder();
    setNewestSmilepackVersion();
}

PidginDialog::~PidginDialog()
{
    delete ui;
}

void PidginDialog::getSmileFolder()
{
    homePath = QDir::homePath();

    #ifdef Q_OS_WIN
        destination = homePath + "/AppData/Roaming/.purple/smileys";
        setSmileLocation(destination);
        emit locationChanged();
    #endif

    #ifdef Q_OS_UNIX
        destination = homePath + "/.purple/smileys";
        setSmileLocation(destination);
        emit locationChanged();
#endif
}

void PidginDialog::disableInstallButton()
{
    ui->installBtn->setEnabled(false);
}

void PidginDialog::setStatusBarText(QString text)
{
    statusBar->showMessage(text);
}

void PidginDialog::setNewestSmilepackVersion()
{
    zipname = jparser.parseJson("https://eddykk.com/api/latest/pidgin", "content");
    if(zipname != NULL)
    {
        version = zipname.mid(6);
        version.chop(11);
        ui->webVersionLabel->setText(version);
        setStatusBarText("Version information updated");
        emit pidginVersionChanged();
    }
}

void PidginDialog::enableInstallButton()
{
    if(ui->pidginLocationLbl->text() != "Not selected :( Press Browse and locate smile folder"
    && ui->webVersionLabel->text() != "Could not connect to EddyKK server")
    {
        ui->installBtn->setEnabled(true);
    }else{
        ui->installBtn->setEnabled(false);
    }
}

void PidginDialog::startExtract(QString target)
{
    qDebug() << "Extracting started";
    setStatusBarText("Extracting started");
    emit startextracting(target);
}

void PidginDialog::startCopying(QString source)
{
    setStatusBarText("Copying started");
    QString finalDestiantion = ui->pidginLocationLbl->text();
    emit startcopying(source, finalDestiantion, false, false);
}

void PidginDialog::setSmileLocation(QString path)
{
    ui->pidginLocationLbl->setText(path);
    setStatusBarText("Pidgin's smile location is set");
}

void PidginDialog::on_versionBtn_clicked()
{
    setNewestSmilepackVersion();
}

void PidginDialog::on_browseBtn_clicked()
{
    dialogDestination = QFileDialog::getExistingDirectory(this, tr("Open smile installation directory"), destination);

    if (dialogDestination != NULL)
    {
        setSmileLocation(dialogDestination);
        emit locationChanged();
    }
    qDebug() << "Destination folder selected: " << dialogDestination;
}

void PidginDialog::on_installBtn_clicked()
{
    disableInstallButton();

    QUrl url = "https://eddykk.com/smiley/pidgin/" + zipname;

    qDebug() << "Installation started";
    setStatusBarText("Installation started");
    emit download(url);
}
