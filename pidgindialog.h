#ifndef PIDGINDIALOG_H
#define PIDGINDIALOG_H

#include "jsonparser.h"
#include "downloader.h"
#include "extractor.h"
#include "copier.h"

#include <QDialog>
#include <QFileDialog>
#include <QStatusBar>

namespace Ui {
class PidginDialog;
}

class PidginDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PidginDialog(QWidget *parent = 0);
    ~PidginDialog();

private:
    Ui::PidginDialog *ui;

    JsonParser jparser;
    Downloader dler;
    Extractor extrer;
    Copier cpier;

    QStatusBar *statusBar;

    QString version;
    QString zipname;
    QString homePath;
    QString destination;
    QString dialogDestination;

    void getSmileFolder();
    void disableInstallButton();

public slots:
    void setStatusBarText(QString);

private slots:
    void on_versionBtn_clicked();
    void on_browseBtn_clicked();
    void on_installBtn_clicked();

    void setNewestSmilepackVersion();
    void enableInstallButton();
    void startExtract(QString);
    void startCopying(QString);
    void setSmileLocation(QString);

signals:
    void pidginVersionChanged();
    void download(QUrl);
    void startextracting(QString);
    void startcopying(QString, QString, bool, bool);
    void locationChanged();
};

#endif // PIDGINDIALOG_H
